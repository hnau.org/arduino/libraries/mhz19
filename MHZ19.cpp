#include "MHZ19.h"
#include <HexUtils.h>

const PROGMEM uint8_t MHZ19::request[requestLength] = {
	0xFF, 0x01, 0x86,
	0x00, 0x00, 0x00,
	0x00, 0x00, 0x79
};

MHZ19::MHZ19(
		uint16_t pinRx, uint16_t pinTx,
		uint32_t speed,
		std::function<timestamp()> getPeriod_,
		std::function<void(uint16_t)> handleResult_,
		Logger& logger_
): sensorTransport(pinRx, pinTx), getPeriod(getPeriod_), handleResult(handleResult_), logger(logger_, FCP("MHZ19")) {
	sensorTransport.begin(speed);
	currentStep = 0;
}

bool MHZ19::step() {
	
	if (currentStep == 0) {
		for (size_t i = 0; i < requestLength; i++) {
			uint8_t requestByte = pgm_read_byte(request+i);
			sensorTransport.write(requestByte);
		}
		return true;
	}
	
	uint8_t responseByteNumber = currentStep - 1;
	
	if (responseByteNumber == 0) {
		if (
			sensorTransport.available() > 0 && 
			(unsigned char)sensorTransport.peek() != 0xFF
		) {
			sensorTransport.read();
			return false;
		}
	}
	
	response[responseByteNumber] = sensorTransport.read();
	
	if (responseByteNumber == responseLength - 1) {
		onResponseReceived();
	}
	
	return true;	
}

uint8_t MHZ19::calcCrc(
	uint8_t* bytes, 
	size_t start, 
	size_t length
) {
	uint8_t crc = 0;
	for (size_t i = start; i < start+length; i++) {
		crc += bytes[i];
	}
	crc = 255 - crc;
	crc++;
	return crc;
}


void MHZ19::logCrcValidatingError(
	uint8_t receivedCrc, 
	uint8_t calculatedCrc
) {
	logger.log([&](Logger& log) { 
		log.log(FCP("Response checksum validating fail. Calculated CRC is "));
		log.log(calculatedCrc);
		log.log(FCP(", but received CRC is "));
		log.log(receivedCrc);
		log.log(FCP(". Response: "));
		char responseString[responseLength*2+1];
		HexUtils::putBytesHexViewToCharsArray(response, responseLength, responseString);
		log.log(CP(responseString));
	});
}

void MHZ19::logUnexpectedByteError(
	uint8_t received, 
	uint8_t expected, 
	uint8_t position
) {
	logger.log([&](Logger& log) { 
		log.log(FCP(("Unexpectet byte on position ")));
		log.log(position);
		log.log(FCP((" in response. Expectet value is ")));
		log.log(expected);
		log.log(FCP((", but received is ")));
		log.log(received);
	});
}

void MHZ19::onResponseReceived() {
	
	if (response[0] != 0xff) {
		logUnexpectedByteError(response[0], 0xff, 0);
		return;
	}
	
	if (response[1] != 0x86) {
		logUnexpectedByteError(response[1], 0x86, 1);
		return;
	}
	
	uint8_t receivedCrc = response[responseLength-1];
	uint8_t calculatedCrc = calcCrc(response, 1, responseLength-1-1);
	
	if (receivedCrc != calculatedCrc) {
		logCrcValidatingError(receivedCrc, calculatedCrc);
		return;
	}
	
	uint8_t high = response[2];
	uint8_t low = response[3];
	uint16_t ppm = high*256 + low;
	handleResult(ppm);
}

timestamp MHZ19::loop() {
	
	bool stepPassed = step();
	if (!stepPassed) {
		return 0;
	}
	
	currentStep++;
	if (currentStep == 1 + responseLength) {
		currentStep = 0;
		return getPeriod();
	}
	
	return 0;
	
}
