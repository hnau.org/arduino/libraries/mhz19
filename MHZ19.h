#ifndef MHZ19_H
#define MHZ19_H

#include <TaggedLogger.h>
#include <Thread.h>
#include <SoftwareSerial.h>

class MHZ19: public Thread {
	
private:

	TaggedLogger logger;

	SoftwareSerial sensorTransport;
	
	std::function<timestamp()> getPeriod;
	std::function<void(uint16_t)> handleResult;
	
	static const uint8_t requestLength = 9;
	static const uint8_t responseLength = 9;
	static const uint8_t request[requestLength];
	
	uint8_t currentStep;
	uint8_t response[responseLength];
	
	bool step();
	void onResponseReceived();
	
	uint8_t calcCrc(uint8_t* bytes, size_t start, size_t length);
	void logCrcValidatingError(uint8_t receivedCrc, uint8_t calculatedCrc);
	void logUnexpectedByteError(uint8_t received, uint8_t expected, uint8_t position);
	
protected:

    virtual timestamp loop() override;

    
public:

	MHZ19(
		uint16_t pinRx, uint16_t pinTx,
		uint32_t speed,
		std::function<timestamp()> getPeriod_,
		std::function<void(uint16_t)> handleResult_,
		Logger& logger_
	);
	
	
};

#endif //MHZ19_H
